package com.example.coursemanagementsystem.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "users")
@NoArgsConstructor
@Getter
@Setter

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="login")
    private String login;

    @Column(name="login")
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name="userType")
    private UserType userType;

    @Column(name="name")
    private String name;

    @Column(name="surname")
    private String surname;

    @Column(name="email")
    private String email;

    @Column(name="active")
    private boolean active = false;

    @OneToMany(mappedBy = "user")
    private Set<UserNotification> userNotificationSet = new HashSet<>();

}
