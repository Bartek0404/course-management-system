package com.example.coursemanagementsystem.model;

import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "lessons")
@NoArgsConstructor
@Getter
@Setter
public class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "topic")
    private String topic;

    @Column(name = "lesson_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate lessonDate;

    @Column(name = "lesson_start_time")
    @DateTimeFormat(pattern = "hh:mm")
    private LocalTime lessonStartTime;

    @Column(name = "lesson_end_time")
    @DateTimeFormat(pattern = "hh:mm")
    private LocalTime lessonEndTime;

    @Column(name = "assigned_to_block")
    private boolean assignedToBlock = false;

    @ManyToOne
    @JoinColumn(name = "lesson_block_id")
    private LessonBlock lessonBlock;

}
