package com.example.coursemanagementsystem.model;

import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "lesson_blocks" )
@NoArgsConstructor
@Getter
@Setter
public class LessonBlock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "block_name")
    private String blockName;

    @Column(name = "assigned_to_course")
    private boolean assignedToCourse = false;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "lessonBlock")
    private Set<Lesson> lessonSet = new HashSet<>();

    @ManyToOne()
    @JoinColumn(name = "example123_id")
    private Course course;

}
