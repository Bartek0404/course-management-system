package com.example.coursemanagementsystem.model;

public enum UserType {
    ADMIN,INSTRUCTOR,STUDENT
}
